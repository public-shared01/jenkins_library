#!/usr/bin/env groovy
// This function offers a choice between las five short commit sha in the repository
// with a timeuot 20 min

def call() {
    List<String> versions = sh(
      returnStdout: true, script: "git log -n 5 --pretty=format:'%h'").trim().split('\n')
    
    println "This is a las five commits and image tags in repository match-engine"
    println versions

    timeout(time: 20, unit: 'MINUTES') { 
    selectedCommit = input(
      id: 'commitSelection', message: 'Select commit to build', 
      parameters: [choice(
        choices: versions, 
        description: 'Please select a SHORT_COMMIT_SHA to build on US PRODUCTION environment', 
        name: 'SHORT_COMMIT_SHA'
        )])
    env.SHORT_COMMIT_SHA = selectedCommit.split(' ')[0]
  }
}
