#!/usr/bin/env groovy
//
// This function offers a choice between las five image tag 
// in the Google Container Registry
// with a timeuot 20 min
// Required installed gcloud CLI tool or running in container 'gcr.io/google.com/cloudsdktool/cloud-sdk'
// You need to define a registry 

def call(String gcrImagePath) {
    echo "Getting last tags from docker container registry '$gcrImagePath'"
    if(gcrImagePath.contains(':')){
        gcrImagePath = gcrImagePath.split(':')[0]
    }
    List<String> imageTags = sh (
      returnStdout: true, 
      script: """
      set -eux
      gcloud container images list-tags '$gcrImagePath' --flatten="[].tags[]" --format="value(tags)" | head -5
    """
    ).trim().split('\n')
    println "This is a las five image tags in registry '$gcrImagePath'"
    println imageTags

    timeout(time: 20, unit: 'MINUTES') { 
    selectedImage = input(
      id: 'commitSelection', message: 'Select image tag to deploy', 
      parameters: [choice(
        choices: imageTags, 
        description: 'Please select a GCR_IMAGE_TAG to build on PRODUCTION environment', 
        name: 'GCR_IMAGE_TAG'
        )])
    env.GCR_IMAGE_TAG = selectedImage.split(' ')[0]
  }
}
