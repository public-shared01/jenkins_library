// This function make short commit messages 
//https://www.jenkins.io/doc/pipeline/examples/

def call () {
  env.GIT_COMMIT_SHORT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
}