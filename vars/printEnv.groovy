#!/usr/bin/env groovy
//
// This function print environment variables to Jenkins log
//


def call(){
  timeout(time: 20, unit: 'SECONDS') {
    String label = 'Existing now environment variables'
    sh (
      label: "$label",
      script: '''
        set -eux
        env | sort
      '''
    )
  }
}