#!/usr/bin/env groovy
// This call checks images for exists and makes a tag if not.
// Should be run gitCommitShort() before
// Required environment variables :
//   DOCKER_IMAGES
//   GIT_COMMIT_SHORT
// 

def call () {
  echo "Get image list from ${DOCKER_IMAGES} registry"
  List imageList = sh(
    returnStdout: true,
    label: 'Checking if the docker image exists in the GCP registry',
    script: """#!/usr/bin/env bash
      set -eux

      gcloud auth list
  
      gcloud container images list-tags ${DOCKER_IMAGES} \
      --filter="tags:${GIT_COMMIT_SHORT}" --format=text
    """).trim().tokenize(',')

  echo "List $imageList"

  if ( ! imageList ) {
    sh "echo 'The image ${GIT_COMMIT_SHORT} does not exist, starting tagging image ${GIT_COMMIT}'"
    sh 'gcloud container images add-tag --quiet "$DOCKER_IMAGES":"$GIT_COMMIT" "$DOCKER_IMAGES":"$GIT_COMMIT_SHORT"'
  } else {
    sh "echo 'Tag ${GIT_COMMIT_SHORT} already exists'"
  }
}